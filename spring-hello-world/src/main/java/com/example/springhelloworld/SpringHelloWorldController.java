
package com.example.springhelloworld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SpringHelloWorldController {

    @GetMapping("/")
    public String index() {
        return "index";
    }
}