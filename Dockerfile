FROM maven:3-eclipse-temurin-17-alpine
WORKDIR /src
COPY . .
RUN mvn clean

RUN mv spring-hello-world/target/spring-*.jar spring-hello-world/target/app.jar
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "spring-hello-world/target/app.jar" ]
